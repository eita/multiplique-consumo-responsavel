<?php 
$head = array('title' => __('Contribution Terms of Service'));
echo head($head);
?>

<h1><?php echo $head['title']; ?></h1>
<div id="primary">
<?php echo get_option('contribution_consent_text'); ?>
</div>
<?php echo foot(); ?>
